/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import djikstra.prim.CrearGrafo;
import djikstra.prim.Logica;
import djikstra.prim.Vertices;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.text.AttributeSet.ColorAttribute;

/**
 *
 * @author ssrs_
 */
public class Ventana extends JFrame implements ActionListener, MouseListener, MouseMotionListener {

	private JPanel pTitulo, pIngreso, pGrafo, pAdyacencia, pSalida;
	private JLabel lTitulo, lDibujo, lAdyacencia, coordenadas,lMinimo;
	private JButton bDijkstra, bPrim, bAdyacencia, bGrafo, bLimpiarP;
	Color black, white, blue,red;
	int x, y, contador, verxs;
	boolean vez;
	private Logica l;
	private ArrayList Vertices;
	private ArrayList<Vertices> v;
	private CrearGrafo c;
	private Graphics g;
	private int[] data;
	private int[][] matrizAd, weight;
	private JTextField leer[][];
	private ArrayList<String> Aristas, Ar;

	JScrollPane scrollMat = new JScrollPane(pAdyacencia, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
			JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

	public Ventana() {
		this.setTitle("Dijkstra-Prim");
		setLayout(null);
		setBounds(30, 30, 1360, 750);
		setLocationRelativeTo(this);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		getContentPane().setBackground(Color.white);
		l = new Logica();
		v = new ArrayList<Vertices>();
		Vertices = new ArrayList();
		vez = true;
		setDatos();
		iniciarComponentes();
	}

	private void iniciarComponentes() {
		Colores();
		Paneles();
		Etiquetas();
		Botones();
	}

	private void Colores() {
		this.black = new Color(0, 0, 0);
		this.blue = new Color(18, 75, 206);
		this.white = new Color(250, 250, 250);
		this.red = new Color(255,0,0);
	}

	private void Paneles() {
		pTitulo = new JPanel();
		pTitulo.setLayout(null);
		pTitulo.setBounds(0, 0, 1350, 50);
		pTitulo.setBackground(this.white);
		this.getContentPane().add(pTitulo);

		pGrafo = new JPanel();
		pGrafo.setLayout(null);
		pGrafo.setBounds(0, 55, 600, 480);
		pGrafo.setBackground(white);
		pGrafo.addMouseListener(this);
		pGrafo.addMouseMotionListener(this);
		pGrafo.setVisible(true);
		pGrafo.setBorder(BorderFactory.createLineBorder(Color.lightGray));
		pIngreso = new JPanel();
		pIngreso.setLayout(null);
		pIngreso.setBounds(0, 50, 600, 700);
		pIngreso.setBackground(white);
		this.getContentPane().add(pIngreso);
		pIngreso.add(pGrafo);
		pSalida = new JPanel();
		pSalida.setBounds(600, 400, 750, 350);
		pSalida.setBackground(white);
		add(pSalida).setVisible(true);
	}

	private void Etiquetas() {
		lTitulo = new JLabel("<html><center>DiJkstra - Prim<center><html>");
		lTitulo.setForeground(black);
		lTitulo.setFont(new Font("Impact", Font.PLAIN, 25));
		lTitulo.setBounds(30, 15, 400, 20);
		pTitulo.add(lTitulo);

		lDibujo = new JLabel("Dibujar grafo");
		lDibujo.setForeground(black);
		lDibujo.setFont(new Font("Impact", Font.PLAIN, 20));
		lDibujo.setBounds(30, 10, 400, 40);
		pIngreso.add(lDibujo);
		
		lMinimo = new JLabel("");
		lMinimo.setForeground(black);
		lMinimo.setFont(new Font("Impact",Font.PLAIN,20));
		lMinimo.setBounds(10,550,250,25);
		pIngreso.add(lMinimo).setVisible(false);
	}

	private void Botones() {
		bDijkstra = new JButton("Dijkstra");
		bDijkstra.setFont(new Font("Impact", Font.PLAIN, 15));
		bDijkstra.setBounds(50, 600, 200, 50);
		bDijkstra.addActionListener(this);
		bDijkstra.setBackground(black);
		bDijkstra.setForeground(white);
		pIngreso.add(bDijkstra).setVisible(false);

		bPrim = new JButton("Prim");
		bPrim.setFont(new Font("Impact", Font.PLAIN, 15));
		bPrim.setBounds(300, 600, 200, 50);
		bPrim.addActionListener(this);
		bPrim.setBackground(black);
		bPrim.setForeground(white);
		pIngreso.add(bPrim).setVisible(false);

		bAdyacencia = new JButton("Adyacencia");
		bAdyacencia.setFont(new Font("Impact", Font.PLAIN, 15));
		bAdyacencia.setBounds(400, 5, 150, 40);
		pIngreso.add(bAdyacencia).setVisible(false);
		bAdyacencia.addActionListener(this);
		bAdyacencia.setBackground(black);
		bAdyacencia.setForeground(white);

		bLimpiarP = new JButton("Limpiar panel");
		bLimpiarP.setFont(new Font("Impact", Font.PLAIN, 15));
		bLimpiarP.setBounds(350, 550, 250, 30);
		pIngreso.add(bLimpiarP).setVisible(false);
		bLimpiarP.addActionListener(this);
		bLimpiarP.setBackground(black);
		bLimpiarP.setForeground(white);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == bDijkstra) {
			System.out.println("Calculando Dijkstra...");
			l.setVertices(v);
			// B_Reinicio.setVisible(true);
			bLimpiarP.setVisible(true);
			matrizAdy();
			l.Dijkstra(0);
			Dibujar d = new Dibujar();
			g = pSalida.getGraphics();
			d.graficarDijkstra(l.getVertices(), l.getMinimos(), l.getMatrizDeAdyacencia(), l.getPadres(), l, g);
		}
		if (e.getSource() == bPrim) {
			System.out.println("Calculando Prim...");
			g = pSalida.getGraphics();
			// B_Reinicio.setVisible(true);
			bLimpiarP.setVisible(true);
			llenarData();
			int f = v.size();
			int h = v.size();
			llenarWeight(f, h);
			verxs = data.length;
			c = new CrearGrafo(verxs, data, weight);
			l.Prim(c, 0);
			ArrayList<String> datosPrim = l.getDatosPrim();
			DibujarPrim(g, datosPrim);
			vez = false;
			lMinimo.setVisible(true);
		}
		if (e.getSource() == bAdyacencia) {
			System.out.println("Adyacencia...");
			metodoMatriz();
			// B_Reinicio.setVisible(true);
		}
		if (e.getSource() == bGrafo) {
			System.out.println("Creando grafo...");
			int f = v.size();
			int c = v.size();
			guardarMatriz(f, c);
			imprimirMatriz();
			Aristas = new ArrayList<>();
			Ar = new ArrayList<>();
			for (Object Arista : Aristas) {
				Ar.add((String) Arista);
			}
			Ar = Aristas;
			llenarAdyacencia();
			imprimirAdyacencia();
			dibujarLineas();
			bDijkstra.setVisible(true);
			bPrim.setVisible(true);
		}
		if (e.getSource() == bLimpiarP) {
			pSalida.repaint();
			Aristas = Ar;
			Aristas = new ArrayList<>();
			for (Object Arista : Ar) {
				Aristas.add((String) Arista);
			}
			data = null;
			weight = null;
			verxs = 0;
			lMinimo.setText("");
			lMinimo.setVisible(false);
		}
	}

	private void DibujarPrim(Graphics g, ArrayList<String> datosPrim) {
		int x, y, valor, minimo = 0;
		contador = 0;
		System.out.println("size v: " + v.size());
		for (int i = 0; i < v.size(); i++) {
			String[] coordenadas = l.getCoordVertices().get(i).split(",");
			x = Integer.parseInt(coordenadas[0]);
			y = Integer.parseInt(coordenadas[1]);
			anadirCirculo2(x, y, g);
		}
		// Valor,xini,yini,xfin,yfin
		g.setColor(black);
		for (int i = 0; i < datosPrim.size(); i++) {
			String[] datos = datosPrim.get(i).split(",");
			x = Integer.parseInt(datos[0]);
			y = Integer.parseInt(datos[1]);
			valor = Integer.parseInt(datos[2]);
			minimo += valor;
			anadirLinea(x, y, valor, g);
		}
		lMinimo.setText("Recubrimiento minimo " + minimo);
		lMinimo.setForeground(blue);
	}

	public void metodoMatriz() {
		int f, c;

		f = v.size();
		c = v.size();

		scrollMat.setBounds(600, 50, 750, 350);
		scrollMat.getHorizontalScrollBar().setCursor(new Cursor(Cursor.HAND_CURSOR));
		scrollMat.getHorizontalScrollBar().setBackground(Color.BLACK);
		scrollMat.getVerticalScrollBar().setCursor(new Cursor(Cursor.HAND_CURSOR));
		scrollMat.getVerticalScrollBar().setBackground(Color.BLACK);

		pAdyacencia = new JPanel();
		scrollMat.setViewportView(pAdyacencia);
		scrollMat.setVisible(true);
		pAdyacencia.setLayout(null);
		pAdyacencia.setBackground(white);
		pAdyacencia.setPreferredSize(new Dimension((c * 100) + 75, (f * 60) + 75));

		add(scrollMat);

		pAdyacencia.setVisible(true);

		lAdyacencia = new JLabel("Matriz de adyacencia");
		lAdyacencia.setForeground(white);
		lAdyacencia.setFont(new Font("consolas", Font.BOLD, 25));
		lAdyacencia.setBounds(30, 15, 400, 20);
		pAdyacencia.add(lAdyacencia).setVisible(true);

		matrizAd = new int[f][c];
		leer = new JTextField[c][f];

		for (y = 0; y < f; y++) {
			for (x = 0; x < c; x++) {
				leer[x][y] = new JTextField();
				pAdyacencia.add(leer[x][y]);
				leer[x][y].setBounds(-50 + (82 * (x + 1)), 40 + (30 * (y + 1)), 50, 20);
				if (x == y) {
					leer[x][y].setText("0");
					leer[x][y].setEditable(false);
				} else if (x < y) {
					leer[x][y].setText("x");
					leer[x][y].setEditable(false);
				} else {
					leer[x][y].setText("99");
				}
				leer[x][y].setBackground(Color.white);

			}
		}
		int posBoton = 40 + (30 * (y + 1));
		bGrafo = new JButton("Crear Grafo");
		bGrafo.setFont(new Font("Impact", Font.PLAIN, 15));
		pAdyacencia.add(bGrafo).setVisible(true);
		bGrafo.addActionListener(this);
		bGrafo.setForeground(white);
		bGrafo.setBackground(black);
		bGrafo.setBounds(20, posBoton, 150, 40);

		for (int i = 0; i < f; i++) {
			coordenadas = new JLabel();
			pAdyacencia.add(coordenadas);
			coordenadas.setText(Integer.toString(i + 1));
			coordenadas.setBounds(5, 40 + (30 * (i + 1)), 50, 20);
		}
		for (int i = 0; i < c; i++) {
			coordenadas = new JLabel();
			pAdyacencia.add(coordenadas);
			coordenadas.setText(Integer.toString(i + 1));
			coordenadas.setBounds(-40 + (82 * (i + 1)), 40, 50, 20);

		}
	}

	public void matrizAdy() {
		int n = v.size();
		int aux[][] = new int[n][n];
		for (int i = 0; i < n; i++) {
			for (int j = i; j < n; j++) {
				try {
					int peso = Integer.parseInt(leer[j][i].getText());
					aux[i][j] = peso;
					aux[j][i] = peso;
				} catch (NumberFormatException e) {

				}

			}
		}

		l.setMatrizDeAdyacencia(aux);
	}

	public boolean guardarMatriz(int f, int c) {
		for (int i = 0; i < f; i++) {
			for (int j = 0; j < c; j++) {
				if (leer[j][i].getText().equals("0") || leer[j][i].getText().equals("x")) {
					matrizAd[i][j] = 0;
				} else {
					matrizAd[i][j] = Integer.parseInt(leer[j][i].getText());
				}
				System.out.print(matrizAd[i][j] + " ");
			}
			System.out.println("");
		}

		return true;
	}

	private void llenarAdyacencia() {
		int nVertices = l.getVertices().size();
		ArrayList<Integer>[] Ady = new ArrayList[nVertices];
		for (int i = 0; i < nVertices; i++) {
			Ady[i] = new ArrayList();
			for (int j = 0; j < nVertices; j++) {
				if (matrizAd[i][j] != 0) {
					Ady[i].add(matrizAd[i][j]);
				}
			}
		}
		l.setAdyacencia(Ady);
	}

	private void imprimirMatriz() {
		System.out.println("MATRIZ:");
		for (int i = 0; i < matrizAd[0].length; i++) {
			for (int j = 0; j < matrizAd.length; j++) {
				System.out.print(matrizAd[i][j] + " ");
			}
			System.out.println("");
		}
	}

	private void imprimirAdyacencia() {
		for (int i = 0; i < l.getVertices().size(); i++) {
			System.out.print((i + 1) + "-> ");
			int lim = l.getAdyacencia()[i].size();
			for (int j = 0; j < lim; j++) {
				System.out.print("[" + l.getAdyacencia()[i].get(j) + "]");
			}
			System.out.println("");
		}
	}

	private void dibujarLineas() {
		g = pGrafo.getGraphics();
		for (int i = 0; i < matrizAd[0].length; i++) {
			for (int j = 0; j < matrizAd.length; j++) {
				if (matrizAd[i][j] != 0 && matrizAd[i][j] != 99) {
					anadirLinea(i + 1, j + 1, matrizAd[i][j], g);
				}
			}
		}
	}

	public void setDatos() {
		contador = 0;
		x = 0;
		y = 0;
		l.setDatos();
		v = new ArrayList<Vertices>();
		data = null;
		weight = null;
		verxs = 0;
	}

	public void llenarData() {
		System.out.println("Data:");
		data = new int[Vertices.size()];
		for (int i = 0; i < Vertices.size(); i++) {
			data[i] = Integer.parseInt(String.valueOf(Vertices.get(i)));
			System.out.print(data[i] + " ");
		}
		System.out.println("");
	}

	public void llenarWeight(int f, int c) {
		System.out.println("WEIGHT:");
		weight = new int[f][c];
		for (int i = 0; i < f; i++) {
			for (int j = 0; j < c; j++) {
				if (leer[j][i].getText().equals("0")) {
					weight[i][j] = 99;
				}
				if (leer[j][i].getText().equals("x")) {
					weight[i][j] = Integer.parseInt(leer[i][j].getText());

				} else {
					weight[i][j] = Integer.parseInt(leer[j][i].getText());
				}
				System.out.print(weight[i][j] + " ");
			}
			System.out.println("");
		}
		l.setAristas(Aristas);
	}

	public void anadirCirculo(int x, int y, Graphics g) {
		contador++;
		g.setColor(Color.blue);
		g.drawOval(x, y, 20, 20);
		g.drawString(String.valueOf(contador), x + 3, y + 15);
		l.getCoordVertices().add(x + "," + y);
		Vertices.add(contador);
		v.add(new Vertices(contador, x, y));
	}

	public void anadirCirculo2(int x, int y, Graphics g) {
		contador++;
		g.setColor(Color.blue);
		g.drawOval(x, y, 20, 20);
		g.drawString(String.valueOf(contador), x + 3, y + 15);
	}

	public void anadirLinea(int x, int y, int valor, Graphics g) {
		String[] cooIn, cooFn;
		int xini = 0, yini = 0, xfin = 0, yfin = 0;
		try {
			for (int i = 0; i < l.getCoordVertices().size(); i++) {
				if (x == (i + 1)) {
					cooIn = l.getCoordVertices().get(i).split(",");
					xini = Integer.parseInt(cooIn[0]);
					yini = Integer.parseInt(cooIn[1]);
				} else if (y == (i + 1)) {
					cooFn = l.getCoordVertices().get(i).split(",");
					xfin = Integer.parseInt(cooFn[0]);
					yfin = Integer.parseInt(cooFn[1]);
				}
			}

			int xComp = xfin - xini;
			if (xComp > 30) {
				int yComp = yfin - yini;
				if (yComp > 30) {
					yini += 20;
				} else if (yComp < -30) {
					yfin += 20;
				} else {
					yini += 5;
					yfin += 5;
				}
				xini += 20;
			} else if (xComp < -30) {
				int yComp = yfin - yini;
				if (yComp > 30) {
					yini += 20;
				} else if (yComp < -30) {
					yfin += 20;
				} else {
					yini += 5;
					yfin += 5;
				}
				xfin += 20;
			} else {
				int yComp = yfin - yini;
				if (yComp > 30) {
					yini += 20;
				} else if (yComp < -30) {
					yfin += 20;
				} else {
					yini += 5;
					yfin += 5;
				}
				xini += 5;
				xfin += 5;
			}
			g.setColor(red);
			g.setFont(new Font("Impact", Font.PLAIN, 14));
			g.drawString(valor + "", (xini + xfin) / 2, (yini + yfin) / 2);
			// g.drawLine(xfin, yfin, xfin, yfin);
			g.setColor(black);
			g.drawLine(xini, yini, xfin, yfin);
			String datos = x + "," + y + "," + valor + "," + xini + "," + xfin + "," + yini + "," + yfin;
			if (vez) {
				Aristas.add(datos);
			}

		} catch (IndexOutOfBoundsException e) {
			JOptionPane.showMessageDialog(null, "No se encontro circulo");
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		if (e.getButton() == 1) {
			bAdyacencia.setVisible(true);
			System.out.println("Evento circulo");
			x = e.getX();
			System.out.println("x es: " + x);
			y = e.getY();
			g = pGrafo.getGraphics();
			anadirCirculo(x, y, g);
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {

	}

	@Override
	public void mouseReleased(MouseEvent e) {

	}

	@Override
	public void mouseEntered(MouseEvent e) {

	}

	@Override
	public void mouseExited(MouseEvent e) {

	}

	@Override
	public void mouseDragged(MouseEvent e) {

	}

	@Override
	public void mouseMoved(MouseEvent e) {

	}
}
